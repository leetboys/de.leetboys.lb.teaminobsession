<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<section class="latestnews_wrapper" id="<?php echo $headerclass; ?>">
    <h2><?php echo $moduleclass_sfx; ?></h2>
<ul class="latestnews <?php echo $moduleclass_sfx; ?>">
<?php foreach ($list as $item) :  ?>
   
	<li itemscope itemtype="http://schema.org/Article">
                <?php echo $item->introtext; ?>
                <?php
                /*
		<a href="<?php echo $item->link; ?>" itemprop="url">
			<span itemprop="name">
				<?php echo $item->title; ?>
			</span>
		</a>
                 * 
                 */
                ?>
	</li>
<?php endforeach; ?>
</ul>
</section>